using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class AdManager
{
    private BannerView bannerView = null;

	bool bAdRequest = false;
	bool bAdLoaded = false;
	bool bAdShow = false;

	public static AdManager instance
	{
		get
		{
			if (_inst == null)
			{
				_inst = new AdManager();
			}
			return _inst;
		}
	}
	static AdManager _inst;

	public void ShowBanner()
	{
		if(bAdLoaded)
		{
			bannerView.Show();
			bAdShow = true;
		}
	}

	public void HideBanner()
	{
		bannerView.Hide();

		if(!bAdRequest || bAdShow)
		{
			bAdShow = false;
			bannerView.LoadAd(createAdRequest());
		}
	}

	public void RequestBanner()
    {
		#if UNITY_EDITOR
            string adUnitId = "unused";
        #elif UNITY_ANDROID
            string adUnitId = "ca-app-pub-9527359661952329/2911688091";
        #elif UNITY_IPHONE
			string adUnitId = "ca-app-pub-9527359661952329/2112718493";
		#else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the bottom of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		bannerView.AdFailedToLoad += HandleAdFailedToLoad;
		bannerView.AdOpened += HandleAdOpened;
		bannerView.AdClosing += HandleAdClosing;
		bannerView.AdClosed += HandleAdClosed;
		bannerView.AdLeftApplication += HandleAdLeftApplication;
        // Load a banner ad.
		bannerView.LoadAd(createAdRequest());
		bannerView.Hide();
    }

    // Returns an ad request with custom ad targeting.
    private AdRequest createAdRequest()
    {
		bAdRequest = true;
		bAdLoaded = false;
        return new AdRequest.Builder()
                //.AddTestDevice(AdRequest.TestDeviceSimulator)
				//.AddTestDevice("500fd942b4b09f7501945434a91c1c0d")
				.AddKeyword("entertainment")
				.SetGender(Gender.Unknown)
                .SetBirthday(new DateTime(1985, 1, 1))
                .TagForChildDirectedTreatment(false)
                .AddExtra("color_bg", "9B30FF")
                .Build();

    }

   
    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
		bAdLoaded = true;
		Debug.Log("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
		bAdRequest = false;
		Debug.Log("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
		Debug.Log("HandleAdOpened event received");
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
		Debug.Log("HandleAdClosing event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
		Debug.Log("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
		Debug.Log("HandleAdLeftApplication event received");
    }

    #endregion
}
