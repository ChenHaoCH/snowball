﻿using UnityEngine;
using System.Collections;

public class AdController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AdManager.instance.RequestBanner();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		AdManager.instance.ShowBanner();
	}

	void OnDisable()
	{
		AdManager.instance.HideBanner();
	}
}
