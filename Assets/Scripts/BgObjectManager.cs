﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BgObjectManager : MonoBehaviour 
{
	public static BgObjectManager instance;
	List<GameObject> objectlist = new List<GameObject>();

	// Use this for initialization
	void Start () 
	{
		instance = this;
		foreach(Transform child in gameObject.transform)
		{
			objectlist.Add(child.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public float GetDistance()
	{
		float posx = 3.4f - Random.Range(30.0f, 60.0f);
		foreach(GameObject obj in objectlist)
		{
			if((obj.transform.localPosition.x - posx) < 4)
			{
				posx -= 8;
			}
		}

		return 3.4f - posx;
	}
}
