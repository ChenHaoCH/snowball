﻿using UnityEngine;
using System.Collections;

public class SoundState : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UpdateState();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		UpdateState();
	}

	public void UpdateState()
	{
		if(PlayerPrefs.HasKey("Sound") && PlayerPrefs.GetInt("Sound") == 0)
		{
			gameObject.transform.FindChild("On").transform.gameObject.SetActive(false);
			gameObject.transform.FindChild("Off").transform.gameObject.SetActive(true);
		}
		else
		{
			gameObject.transform.FindChild("On").transform.gameObject.SetActive(true);
			gameObject.transform.FindChild("Off").transform.gameObject.SetActive(false);

		}
	}
}
