using UnityEngine;
using System.Collections;

public class TerrainController : MonoBehaviour
{
	Vector3 start;
	Vector3 end;
	Vector3 speed;
	
	// Use this for initialization
	void Start () {

		start = new Vector3(-50.6f, -10.7f, 0);
		end = new Vector3(101.2f, 2f, 0);
		speed = new Vector3(10, 10*Mathf.Sin(7*3.14f/180.0f), 0);
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!Game.instance.IsPlaying())
			return;
		
		gameObject.transform.localPosition += speed *  Game.instance.speed * Time.deltaTime;
		
		if(gameObject.transform.localPosition.x < end.x)
			return;

		gameObject.transform.localPosition = start;
	}

}

