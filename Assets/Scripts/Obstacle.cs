﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	Vector3 speed;
	bool bRecordScore = false;

	// Use this for initialization
	void Start () {

		speed = new Vector3(10, 10*Mathf.Sin(7*3.14f/180.0f), 0);
	}
	
	void Update () {

		if(!Game.instance.IsPlaying())
			return;

		if(!gameObject.activeSelf)
			return;

		gameObject.transform.localPosition += speed *  Game.instance.speed * Time.deltaTime;
		
		if(gameObject.transform.localPosition.x > 25f)
		{
			gameObject.SetActive(false);
		}

		if(!bRecordScore && gameObject.transform.localPosition.x > 3.4f)
		{
			Game.instance.AddScore();
			bRecordScore = true;
		}
	}

	public void ResetPosition(int pos)
	{
		gameObject.SetActive(true);
		float distance = Random.Range(16.0f, 18.5f);
		float yoriginpos = (pos == 0) ? -0.8f : -3f;
		gameObject.transform.localPosition = new Vector3(3.4f - distance, yoriginpos - distance*Mathf.Sin(7*3.14f/180.0f), -1);
		bRecordScore = false;
	}
}
