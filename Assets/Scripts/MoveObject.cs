﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {

	Vector3 start;
	Vector3 end;
	Vector3 speed;

	// Use this for initialization
	void Start () {

		float distance = Random.Range(30, 60);
		start = new Vector3(3.4f - distance, 0.9f - distance*Mathf.Sin(7*3.14f/180.0f), 10);
		end = new Vector3(15, 1f, 10);
		speed = new Vector3(5f, 5*Mathf.Sin(7*3.14f/180.0f), 0);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(!Game.instance.IsPlaying())
			return;

		gameObject.transform.localPosition += speed *  Game.instance.speed * Time.deltaTime;

		if(gameObject.transform.localPosition.x < end.x)
			return;

		float distance = BgObjectManager.instance.GetDistance();
		start = new Vector3(3.4f - distance, 0.9f - distance*Mathf.Sin(7*3.14f/180.0f), 10);

		gameObject.transform.localPosition = start;

	}

}
