using UnityEngine;
using System.Collections;

public class RockObstacle : MonoBehaviour
{
	public float ypos = -0.8f;
	Vector3 speed;
	bool bRecordScore = false;
	
	// Use this for initialization
	void Start () {
		
		speed = new Vector3(-10, -10*Mathf.Sin(7*3.14f/180.0f), 0);
		gameObject.SetActive(false);
	}

	
	void Update () {
		
		if(!Game.instance.IsPlaying())
		{
			return;
		}
		
		if(!gameObject.activeSelf)
			return;
		
		gameObject.transform.localPosition += speed *  Game.instance.speed * Time.deltaTime;
		
		if(gameObject.transform.localPosition.x < -10f)
		{
			gameObject.SetActive(false);
		}
		
		if(!bRecordScore && gameObject.transform.localPosition.x < 3.4f)
		{
			Game.instance.AddScore();
			Game.instance.AddScore();
			bRecordScore = true;
		}
	}
	
	public void ResetPosition()
	{
		if(!Game.instance.IsPlaying() || gameObject.activeSelf)
			return;

		gameObject.SetActive(true);
		float distance = Random.Range(16.0f, 40.0f);
		gameObject.transform.localPosition = new Vector3(3.4f + distance, ypos + distance*Mathf.Sin(7*3.14f/180.0f), -1);
		bRecordScore = false;
	}

	public void GameStart()
	{
		gameObject.SetActive(false);
		InvokeRepeating("ResetPosition", 10.0f, 2.5f);
	}

	public void GameEnd()
	{
		gameObject.SetActive(false);
		CancelInvoke();
	}
}

